/* Recupere el monto total (invoices, orders, order_details, products) y la
cantidad de facturas (invoices) por vendedor (employee). Debe
considerar solamente las ordenes con estado diferente de 0 y
solamente los detalles en estado 2 y 3, debe utilizar el precio
unitario de las lineas de detalle de orden, no considere el descuento,
no considere los impuestos, porque la comisión a los vendedores se
paga sobre el precio base. */

desc invoices;
desc orders;
desc order_details;
desc products;

SELECT count(1) as `Cantidad`,
	   concat(e.first_name, ' ', e.last_name) as `Vendedor`,
       round(sum(od.quantity * od.unit_price), 2) as `Monto facturado`
	FROM northwind.order_details od
    JOIN northwind.orders o
	  ON o.id = od.order_id
	JOIN northwind.invoices i
	  ON i.order_id = o.id
	JOIN northwind.products p
	  ON p.id = od.product_id
	JOIN northwind.employees e
	  ON e.id = o.employee_id
      WHERE o.status_id <> 0
		AND od.status_id IN (2, 3) 
      GROUP BY e.id
      ORDER BY `Cantidad` DESC, `Monto facturado` DESC;