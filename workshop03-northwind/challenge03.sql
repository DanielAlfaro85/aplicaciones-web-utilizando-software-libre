/* Recupere la lista de los 10 productos más ordenados (order_details),
y la cantidad total de unidades ordenadas para cada uno de los
productos. */

desc northwind.order_details;
desc northwind.products;

SELECT p.product_code as `Codigo`,
	   p.product_name as `Descripción`,
       round(sum(od.quantity), 2) as `Cantidad`
	FROM northwind.order_details od
    JOIN northwind.products p
	  ON p.id = od.product_id
	GROUP BY p.id
    ORDER BY sum(od.quantity) DESC;