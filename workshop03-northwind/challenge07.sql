/* Genere un reporte de movimientos de inventario
(inventory_transactions) por producto (products), tipo de transacción y
fecha, entre las fechas 22/03/2006 y 24/03/2006 (incluyendo ambas
fechas). */

desc inventory_transactions;
desc inventory_transaction_types;
desc products;

SELECT p.product_code as `Codigo`,
	   p.product_name as `Producto`,
       it.type_name as `Tipo`,
	   DATE_FORMAT(i.transaction_created_date, "%d/%m/%Y") AS `Fecha`,
       sum(i.quantity) as `Cantidad`
	FROM inventory_transactions i
    JOIN inventory_transaction_types it
	  ON it.id = i.transaction_type
    JOIN products p
	  ON p.id = i.product_id
      WHERE i.transaction_created_date BETWEEN '2006/03/22' AND '2006/03/24'
	GROUP BY p.product_name, it.type_name DESC, i.transaction_created_date ;
