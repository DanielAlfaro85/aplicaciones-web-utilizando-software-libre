/* Recupere los movimientos de inventario del tipo salida. Tomando
como base todos los movimientos de inventario
(inventory_transactions), considere unicamente los tipos de
movimiento (transaction_type) 2, 3 y 4 como salidas. */


desc inventory_transactions;
desc products;

SELECT p.product_code as `Codigo`,
	   p.product_name as `Producto`,
       sum(i.quantity) as `Cantidad`
	FROM inventory_transactions i
    JOIN products p
	  ON p.id = i.product_id
      WHERE i.transaction_type IN (2, 3, 4)
	GROUP BY i.product_id;
