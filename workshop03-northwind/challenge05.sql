/* Recupere los movimientos de inventario del tipo ingreso. Tomando
como base todos los movimientos de inventario
(inventory_transactions), considere unicamente el tipo de movimiento
1 (transaction_type) como ingreso. */

desc inventory_transactions;
desc products;

SELECT p.product_code as `Codigo`,
	   p.product_name as `Producto`,
       sum(i.quantity) as `Cantidad`
	FROM inventory_transactions i
    JOIN products p
	  ON p.id = i.product_id
      WHERE i.transaction_type = 1
	GROUP BY i.product_id;