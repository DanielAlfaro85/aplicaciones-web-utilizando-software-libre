/* Genere la consulta SQL para un reporte de inventario, tomando como base todos los
movimientos de inventario (inventory_transactions), considere los tipos de movimiento
(transaction_type) 2, 3 y 4 como salidas y el tipo 1 como ingreso. */

desc inventory_transactions;
desc inventory_transaction_types;
desc products;

SELECT p.product_code as `Codigo`,
	   p.product_name as `Producto`,
       IF(i.transaction_type = 1, i.quantity, "" ) AS `Ingresos`,
	   IF(i.transaction_type = 2 or 3 or 4, sum(i.quantity)-i.quantity, "") as `Salidas`,
	   IF(sum(i.quantity)-i.quantity - i.quantity < 0, (sum(i.quantity)-i.quantity - i.quantity) * -1, sum(i.quantity)-i.quantity - i.quantity) as `Disponible`
	FROM inventory_transactions i
    JOIN products p
	  ON p.id = it.product_id
	GROUP BY p.product_name;
    
    