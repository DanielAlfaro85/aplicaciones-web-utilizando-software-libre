/* Recupere la cantidad de ordenes (orders)
registradas por cada vendedor (employees). */




desc northwind.orders;
desc northwind.employees;

SELECT concat(e.first_name, ' ', e.last_name) as `Vendedor`,
	   count(1) as `Cantidad`
	FROM northwind.orders o
    JOIN northwind.employees e
	  ON e.id = o.employee_id
	GROUP BY e.id;