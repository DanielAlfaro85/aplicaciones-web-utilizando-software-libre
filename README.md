# Workshop 03 - SQL con Northwind

### Challenge 1
Recupere el código (id) y la descripción (type_name) de los tipos de
movimiento de inventario (inventory_transaction_types).

```sql
desc inventory_transaction_types;

SELECT id as `Código`, 
		type_name as `Descripción`
        FROM northwind.inventory_transaction_types;
```
![Captura_de_pantalla__53_](/uploads/da2bdc7f1b9051d4b7b83d2db4de1cc1/Captura_de_pantalla__53_.png)


### Challenge 2
Recupere la cantidad de ordenes (orders)
registradas por cada vendedor (employees).


```sql
desc northwind.orders;
desc northwind.employees;

SELECT concat(e.first_name, ' ', e.last_name) as `Vendedor`,
	   count(1) as `Cantidad`
	FROM northwind.orders o
    JOIN northwind.employees e
	  ON e.id = o.employee_id
	GROUP BY e.id;
```
![Captura_de_pantalla__54_](/uploads/20465eda805e14b942c4ec989662bb8e/Captura_de_pantalla__54_.png)


### Challenge 3
Recupere la lista de los 10 productos más ordenados (order_details),
y la cantidad total de unidades ordenadas para cada uno de los
productos.

```sql
desc northwind.order_details;
desc northwind.products;

SELECT p.product_code as `Codigo`,
	   p.product_name as `Descripción`,
       round(sum(od.quantity), 2) as `Cantidad`
	FROM northwind.order_details od
    JOIN northwind.products p
	  ON p.id = od.product_id
	GROUP BY p.id
    ORDER BY sum(od.quantity) DESC;
```
![Captura_de_pantalla__55_](/uploads/4a7e18e011e68c61520d1b86de31530f/Captura_de_pantalla__55_.png)


### Challenge 4
considerar solamente las ordenes con estado diferente de 0 y
solamente los detalles en estado 2 y 3, debe utilizar el precio
unitario de las lineas de detalle de orden, no considere el descuento,
no considere los impuestos, porque la comisión a los vendedores se
paga sobre el precio base.

```sql
desc invoices;
desc orders;
desc order_details;
desc products;

SELECT count(1) as `Cantidad`,
	   concat(e.first_name, ' ', e.last_name) as `Vendedor`,
       round(sum(od.quantity * od.unit_price), 2) as `Monto facturado`
	FROM northwind.order_details od
    JOIN northwind.orders o
	  ON o.id = od.order_id
	JOIN northwind.invoices i
	  ON i.order_id = o.id
	JOIN northwind.products p
	  ON p.id = od.product_id
	JOIN northwind.employees e
	  ON e.id = o.employee_id
      WHERE o.status_id <> 0
		AND od.status_id IN (2, 3) 
      GROUP BY e.id
      ORDER BY `Cantidad` DESC, `Monto facturado` DESC;
```
![Captura_de_pantalla__56_](/uploads/dcb2b4db17bb74c93c0371402d75b53f/Captura_de_pantalla__56_.png)


### Challenge 5
Recupere los movimientos de inventario del tipo ingreso. Tomando
como base todos los movimientos de inventario
(inventory_transactions), considere unicamente el tipo de movimiento
1 (transaction_type) como ingreso.

```sql
desc inventory_transactions;
desc products;

SELECT p.product_code as `Codigo`,
	   p.product_name as `Producto`,
       sum(i.quantity) as `Cantidad`
	FROM inventory_transactions i
    JOIN products p
	  ON p.id = i.product_id
      WHERE i.transaction_type = 1
	GROUP BY i.product_id;
```
![Captura_de_pantalla__58_](/uploads/22ebdfce6dc0fb0adaf725223c55fa6f/Captura_de_pantalla__58_.png)


### Challenge 6
Recupere los movimientos de inventario del tipo salida. Tomando
como base todos los movimientos de inventario
(inventory_transactions), considere unicamente los tipos de
movimiento (transaction_type) 2, 3 y 4 como salidas.

```sql
desc inventory_transactions;
desc products;

SELECT p.product_code as `Codigo`,
	   p.product_name as `Producto`,
       sum(i.quantity) as `Cantidad`
	FROM inventory_transactions i
    JOIN products p
	  ON p.id = i.product_id
      WHERE i.transaction_type IN (2, 3, 4)
	GROUP BY i.product_id;
```
![Captura_de_pantalla__59_](/uploads/7c3c9dcd37b29b6abd746431fb992b8f/Captura_de_pantalla__59_.png)


### Challenge 7
Genere un reporte de movimientos de inventario
(inventory_transactions) por producto (products), tipo de transacción y
fecha, entre las fechas 22/03/2006 y 24/03/2006 (incluyendo ambas
fechas).

```sql
desc inventory_transactions;
desc inventory_transaction_types;
desc products;

SELECT p.product_code as `Codigo`,
	   p.product_name as `Producto`,
       it.type_name as `Tipo`,
	   DATE_FORMAT(i.transaction_created_date, "%d/%m/%Y") AS `Fecha`,
       sum(i.quantity) as `Cantidad`
	FROM inventory_transactions i
    JOIN inventory_transaction_types it
	  ON it.id = i.transaction_type
    JOIN products p
	  ON p.id = i.product_id
      WHERE i.transaction_created_date BETWEEN '2006/03/22' AND '2006/03/24'
	GROUP BY p.product_name, it.type_name DESC, i.transaction_created_date ;
```
![Captura_de_pantalla__60_](/uploads/6306be5b5610fa67a0ced6c6c982f416/Captura_de_pantalla__60_.png)


### Challenge 8
Genere la consulta SQL para un reporte de inventario, tomando como base todos los
movimientos de inventario (inventory_transactions), considere los tipos de movimiento
(transaction_type) 2, 3 y 4 como salidas y el tipo 1 como ingreso.

```sql
desc inventory_transactions;
desc inventory_transaction_types;
desc products;

SELECT p.product_code as `Codigo`,
	   p.product_name as `Producto`,
       IF(i.transaction_type = 1, i.quantity, "" ) AS `Ingresos`,
	   IF(i.transaction_type = 2 or 3 or 4, sum(i.quantity)-i.quantity, "") as `Salidas`,
	   IF(sum(i.quantity)-i.quantity - i.quantity < 0, (sum(i.quantity)-i.quantity - i.quantity) * -1, sum(i.quantity)-i.quantity - i.quantity) as `Disponible`
	FROM inventory_transactions i
    JOIN products p
	  ON p.id = i.product_id
	GROUP BY i.product_id;
```
![Captura_de_pantalla__61_](/uploads/fb4cf89ef541f56150944b23b5036879/Captura_de_pantalla__61_.png)
    
